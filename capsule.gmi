# Capsule Gemini in italiano

Questo è un elenco in ordine alfabetico di capsule in lingua italiana (anche in più lingue tra cui l'italiano):

=> gemini://alsd.eu/it/
=> gemini://andreafeletto.com/
=> gemini://cobradile.pollux.casa/index_it.gmi
=> gemini://fmpoerio.eu/
=> gemini://gemini-italia.europecom.net/
=> gemini://gemini.barca.mi.it/
=> gemini://gemini.iosa.it/
=> gemini://head.baselab.org/
=> gemini://it.omarpolo.com/
=> gemini://tracciabi.li/whiterabbit/
=> gemini://vidage.rocks/

Se volete essere aggiunti chiedete sul canale IRC o proponete modifiche al repository della documentazione.

Ultimo aggiornamento 2021-04-23
